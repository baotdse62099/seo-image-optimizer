﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageAltOptimizer.Models
{
    public class Variant
    {

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("product_id")]
        public long ProductId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("sku")]
        public string SKU { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("inventory_policy")]
        public string InventoryPolicy { get; set; }

        [JsonProperty("compare_at_price")]
        public object CompareAtPrice { get; set; }

        [JsonProperty("fulfillment_service")]
        public string FulfillmentService { get; set; }

        [JsonProperty("inventory_management")]
        public object InventoryManagement { get; set; }

        [JsonProperty("option1")]
        public string Option1 { get; set; }

        [JsonProperty("option2")]
        public object Option2 { get; set; }

        [JsonProperty("option3")]
        public object Option3 { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("taxable")]
        public bool Taxable { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("grams")]
        public int Grams { get; set; }

        [JsonProperty("image_id")]
        public object ImageId { get; set; }

        [JsonProperty("inventory_quantity")]
        public int InventoryQuantity { get; set; }

        [JsonProperty("weight")]
        public double Weight { get; set; }

        [JsonProperty("weight_unit")]
        public string WeightUnit { get; set; }

        [JsonProperty("old_inventory_quantity")]
        public int OldInventoryQuantity { get; set; }

        [JsonProperty("requires_shipping")]
        public bool RequiresShipping { get; set; }
    }

    public class Option
    {

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("product_id")]
        public long ProductId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("values")]
        public IList<string> ValueList { get; set; }
    }

    public class Image
    {

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("product_id")]
        public long ProductId { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("src")]
        public string Src { get; set; }

        [JsonProperty("variant_ids")]
        public IList<object> VariantIdList { get; set; }
    }

    public class ShopifyProductModel
    {

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("body_html")]
        public string BodyHtml { get; set; }

        [JsonProperty("vendor")]
        public string Vendor { get; set; }

        [JsonProperty("product_type")]
        public string ProductType { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreateAt { get; set; }

        [JsonProperty("handle")]
        public string Handle { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdateAt { get; set; }

        [JsonProperty("published_at")]
        public DateTime PublishAt { get; set; }

        [JsonProperty("template_suffix")]
        public object TemplateSuffix { get; set; }

        [JsonProperty("published_scope")]
        public string PublishScope { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("variants")]
        public IList<Variant> Variants { get; set; }

        [JsonProperty("options")]
        public IList<Option> Options { get; set; }

        [JsonProperty("images")]
        public IList<Image> Images { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }
    }

    public class ProductListJsonModel
    {
        [JsonProperty("products")]
        public List<ShopifyProductModel> ProductList { get; set; }
    }
}