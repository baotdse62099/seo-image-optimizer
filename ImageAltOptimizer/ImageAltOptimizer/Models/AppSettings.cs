﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ImageAltOptimizer.Models
{
    public static class AppSettings
    {
        public static string ApiKey { get; } = ConfigurationManager.AppSettings.Get("ImageAltApiKey");
        public static string SecretKey { get; } = ConfigurationManager.AppSettings.Get("ImageAltSecretKey");
        public static string AppUrl { get; } = ConfigurationManager.AppSettings.Get("ImageAltAppUrl");
        public static string AppScope { get; } = ConfigurationManager.AppSettings.Get("ImageAltScope");
    }
}