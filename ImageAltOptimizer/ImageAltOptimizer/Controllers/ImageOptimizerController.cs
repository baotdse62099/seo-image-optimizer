﻿using ImageAltOptimizer.Data;
using ImageAltOptimizer.Models;
using ImageAltOptimizer.Shopify;
using Newtonsoft.Json;
using ShopifyAPIAdapterLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImageAltOptimizer.Controllers
{
    public class ImageOptimizerController : Controller
    {
        ShopifyAPIClient shopify;

        [HttpPost]
        public ActionResult UpdateTemplate(string template)
        {
            try
            {
                using (var context = new ImageOptimizerDBEntities())
                {
                    var store = context.Stores.Where(q => q.StoreName == shopify.State.ShopName && q.Active).FirstOrDefault();

                    if (store == null)
                    {
                        return Json(new { success = false, message = "App was install incorrectly. Please reinstall the app." });
                    }
                    else
                    {
                        store.AltTemplate = template;
                        context.SaveChanges();
                        return Json(new { success = true, message = "Template updated" });
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, e.Message });
            }
        }

        [HttpPost]
        public ActionResult SyncProductImage()
        {
            try
            {
                var productList = GetAllProducts();

                using (var context = new ImageOptimizerDBEntities())
                {
                    var store = context.Stores.Where(q => q.StoreName == shopify.State.ShopName && q.Active).FirstOrDefault();
                    var template = "";

                    if (store == null)
                    {
                        return Json(new { success = false, message = "App was install incorrectly. Please reinstall the app." });
                    }
                    else
                    {
                        template = store.AltTemplate != null ? store.AltTemplate : ConfigurationManager.AppSettings.Get("DefaultTemplate");
                    }

                    foreach (var product in productList)
                    {
                        var altValue = template
                            .Replace("[product_title]", product.Title)
                            .Replace("[product_type]", product.ProductType)
                            .Replace("[product_vendor]", product.Vendor)
                            .Replace("[shop_name]", shopify.State.ShopName);

                        foreach (var image in product.Images)
                        {

                            var metafieldList = JsonConvert.DeserializeObject<MetafieldListJsonModel>(shopify.Get($"/admin/metafields.json?metafield[owner_id]={image.Id}&metafield[owner_resource]=product_image").ToString());

                            var altMetafield = metafieldList.MetafieldList.Where(q => q.Key == "alt").FirstOrDefault();

                            if (altMetafield == null)
                            {
                                //Create metafield
                                shopify.Post("/admin/metafields.json", new
                                {
                                    metafield = new
                                    {
                                        @namespace = "tags",
                                        key = "alt",
                                        value = altValue,
                                        value_type = "string",
                                        owner_id = image.Id,
                                        owner_resource = "product_image"
                                    }
                                });
                            }
                            else
                            {
                                //Update metafield
                                shopify.Put($"/admin/metafields/{altMetafield.Id}.json", new
                                {
                                    metafield = new
                                    {
                                        id = altMetafield.Id, //metafield id
                                        value = altValue,
                                        value_type = "string"
                                    }
                                });
                            }
                        }
                    }
                }

                return Json(new { success = true, message = "All product images are synced!" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            ShopifyAuthorizationState authState = ShopifyAuthorize.GetAuthorizationState(this.HttpContext);
            if (authState != null)
            {
                shopify = new ShopifyAPIClient(authState, new JsonDataTranslator());
            }
        }

        private List<ShopifyProductModel> GetAllProducts()
        {
            var productList = JsonConvert.DeserializeObject<ProductListJsonModel>(shopify.Get("/admin/products.json").ToString());

            return productList.ProductList;
        }
    }
}