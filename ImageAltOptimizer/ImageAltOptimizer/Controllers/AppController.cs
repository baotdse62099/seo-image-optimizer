﻿using ImageAltOptimizer.Data;
using ImageAltOptimizer.Models;
using ShopifyAPIAdapterLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImageAltOptimizer.Controllers
{
    public class AppController : Controller
    {
        private string apiKey = AppSettings.ApiKey;
        private string secretKey = AppSettings.SecretKey;
        private string appUrl = AppSettings.AppUrl;
        private string appScope = AppSettings.AppScope;

        public ActionResult Install(string shop)
        {
            string shopName = shop.Replace(".myshopify.com", String.Empty);

            // prepare the URL that will be executed after authorization is requested
            Uri requestUrl = this.Url.RequestContext.HttpContext.Request.Url;
            Uri returnURL = new Uri($"{appUrl}{this.Url.Action("AuthenticateCallback", "App")}");

            using (var context = new ImageOptimizerDBEntities())
            {
                var store = context.Stores.Where(q => q.StoreName == shopName && q.Active).FirstOrDefault();

                if (store == null)
                {
                    var authorizer = new ShopifyAPIAuthorizer(shopName, apiKey, secretKey);
                    var authUrl = authorizer.GetAuthorizationURL(new string[] { appScope }, returnURL.ToString());
                    return Redirect(authUrl);
                }
                else
                {
                    return RedirectToAction("AuthenticateCallback", new { shop = shopName, code = "authenticated" });
                }
            }
        }

        public ActionResult AuthenticateCallback(string shop, string code, string error)
        {
            if (!String.IsNullOrEmpty(error))
            {
                this.TempData["Error"] = error;
                return RedirectToAction("Install", new { shop = shop });
            }

            if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(shop))
                return View("Index");

            var shopName = shop.Replace(".myshopify.com", String.Empty);
            var authorizer = new ShopifyAPIAuthorizer(shopName, apiKey, secretKey);

            try
            {
                using (var context = new ImageOptimizerDBEntities())
                {
                    ShopifyAuthorizationState authState = null;
                    var store = context.Stores.Where(q => q.StoreName == shopName && q.Active).FirstOrDefault();

                    if (store == null)
                    {
                        authState = authorizer.AuthorizeClient(code);
                        if (authState != null && authState.AccessToken != null)
                        {
                            context.Stores.Add(new Store
                            {
                                Token = authState.AccessToken,
                                InstallDate = DateTime.Now,
                                StoreName = shopName,
                                Active = true
                            });

                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        authState = new ShopifyAuthorizationState
                        {
                            ShopName = shopName,
                            AccessToken = store.Token
                        };
                    }

                    Shopify.ShopifyAuthorize.SetAuthorization(this.HttpContext, authState);
                }
            }
            catch (Exception e)
            {
                return View("InstallFailed");
            }

            return View("Index");
        }
    }
}